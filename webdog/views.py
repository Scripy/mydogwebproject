from django.http import *
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth import logout
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect, render
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordChangeForm

def page_index(request):
    #context = RequestContext(request)
    template = loader.get_template('index1.html')
    return HttpResponse(template.render())

def page_login(request):
    template = loader.get_template('newlogin.html')
    return HttpResponse(template.render())

def proc_login(request):
    user_name = request.GET['user_name']
    user_password = request.GET['user_password']
    user = authenticate(username = user_name, password = user_password)
    if user is not None:
        if user.is_active:
            login(request, user)
            template = loader.get_template('welcome.html')
            return HttpResponse(template.render())
        else:
            return HttpResponseRedirect("/login/")
    return HttpResponseRedirect("/login/")

def logout_view(request):
    logout(request)
    template = loader.get_template('goodbye.html')
    return HttpResponse(template.render())

def success(request):
    template = loader.get_template('success.html')
    return HttpResponse(template.render())
#старая версия регистрации
class RegisterFormView(FormView):
    form_class = UserCreationForm
    success_url = "/success/"
    template_name = "register.html"

    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self).form_valid(form)
#новая версия регистрации
def reg(request):
    template = loader.get_template('newregistration.html')
    return HttpResponse(template.render())

def dispatch(request):
    username = request.GET['username']
    email = request.GET['email']
    password = request.GET['password']
    password2 = request.GET['password2']
    if password == password2:
        User.objects.create_user(username, email, password)
        template = loader.get_template('success.html')
        return HttpResponse(template.render())

def change_pass(request):
    template = loader.get_template('changePassword.html')
    return HttpResponse(template.render())

def change(request):
    #u = User.objects.get(username=request.user)
    username = request.GET['username']
    old_password = request.GET['password']
    new_pass = request.GET['password2']
    u = authenticate(username=username, password=old_password)
    if u.password == old_password:
        u.set_password(new_pass)
        u.save()
        template = loader.get_template('suc.html')
        return HttpResponse(template.render())
    else:
        template = loader.get_template('notsuc.html')
        return HttpResponse(template.render())

def facts(request):
    template = loader.get_template('fact.html')
    return HttpResponse(template.render())

