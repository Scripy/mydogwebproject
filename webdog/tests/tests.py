#python ./manage.py test
from django.test import TestCase
from webdog.models import Pets, Breeds, Vaccination_certificate, \
    Vaccination_certificate_of_pet, Doctors, Clinic, Reception, Message
from datetime import date
import time
import webdog.models

class PetsTests(TestCase):
    """
    тест модели Pets
    """

    def test_str(self):
        breed = Breeds(name='Pug')
        pet = Pets(name='Rita', breed=breed, date_birth="2013-09-01")
        self.assertEquals(
            str(pet),
            'Rita Pug 2013-09-01',
        )

    def test_get_age(self):
        breed = Breeds(name='Pug')
        pet = Pets(name='Rita', breed=breed, date_birth=date(2013, 9, 1))
        self.assertEquals(
            pet.get_age(),
            7,
        )

    def test_dog_class_by_age(self):
        breed = Breeds(name='Pug')
        pet = Pets(name='Rita', breed=breed, date_birth=date(2013, 9, 1))
        self.assertEquals(
            pet.dog_class_by_age(),
            'open',
        )

    def test_name_max_length(self):
        breed = Breeds(name='Pug')
        pet = Pets(name='Rita', breed=breed, date_birth=date(2013, 9, 1))
        max_length = pet._meta.get_field('name').max_length
        self.assertEquals(max_length, 30)

class BreedsTests(TestCase):
    """
    тест модели Breeds
    """

    def test_all_information_breed(self):
        breed = Breeds(name='Husky', weight=15, height=55)
        self.assertEquals(
            breed.all_information_breed(),
            'Husky 15 55 ',
        )

    def test_name_max_length(self):
        breed = Breeds(name='Husky', weight=15, height=55)
        max_length = breed._meta.get_field('name').max_length
        self.assertEquals(max_length, 30)

class Vaccination_certificateTests(TestCase):
    """
    тест модели Vaccination_certificate
    """

    def test_str(self):
        vaccination = Vaccination_certificate(name='Rabies', age=1)
        self.assertEquals(
            str(vaccination),
            'Rabies 1',
        )

    def test_name_max_length(self):
        vaccination = Vaccination_certificate(name='Rabies', age=1)
        max_length = vaccination._meta.get_field('name').max_length
        self.assertEquals(max_length, 30)


class Vaccination_certificate_of_petTests(TestCase):
    """
    тест модели Vaccination_certificate_of_pet
    """

    def test_str(self):
        vaccination = Vaccination_certificate(name='Rabies', age=1)
        breed = Breeds(name='Pug')
        pet = Pets(name='Rita', breed=breed, date_birth=date(2013, 9, 1))
        patient = Vaccination_certificate_of_pet(name=pet, vaccination=vaccination)
        self.assertEquals(
            str(patient),
            'Rita Pug 2013-09-01 Rabies 1',
        )

class DoctorsTests(TestCase):
    """
    тест модели Doctors
    """

    def test_str(self):
        doctor = Doctors(name='Pork Alexey', specialty='optometrist', number_phone = 8948385858)
        self.assertEquals(
            str(doctor),
            'Pork Alexey optometrist 8948385858',
        )

    def test_name_max_length(self):
        doctor = Doctors(name='Pork Alexey', specialty='optometrist', number_phone = 8948385858)
        max_length = doctor._meta.get_field('name').max_length
        self.assertEquals(max_length, 100)

    def test_specialty_max_length(self):
        doctor = Doctors(name='Pork Alexey', specialty='optometrist', number_phone = 8948385858)
        max_length = doctor._meta.get_field('specialty').max_length
        self.assertEquals(max_length, 30)


class ClinicTests(TestCase):
    """
    тест модели Clinic
    """

    def test_str(self):
        clinic = Clinic(name='Hatiko', city='Ekaterinburg', adress='Posadskay',
                        number_phone = 893586859, email = 'hatiko@yandex.ru')
        self.assertEquals(
            str(clinic),
            'Hatiko Ekaterinburg Posadskay 893586859 hatiko@yandex.ru',
        )

    def test_name_max_length(self):
        clinic = Clinic(name='Hatiko', city='Ekaterinburg', adress='Posadskay',
                        number_phone = 893586859, email = 'hatiko@yandex.ru')
        max_length = clinic._meta.get_field('name').max_length
        self.assertEquals(max_length, 100)

    def test_city_max_length(self):
        clinic = Clinic(name='Hatiko', city='Ekaterinburg', adress='Posadskay',
                        number_phone = 893586859, email = 'hatiko@yandex.ru')
        max_length = clinic._meta.get_field('city').max_length
        self.assertEquals(max_length, 100)
    def test_adress_max_length(self):
        clinic = Clinic(name='Hatiko', city='Ekaterinburg', adress='Posadskay',
                        number_phone = 893586859, email = 'hatiko@yandex.ru')
        max_length = clinic._meta.get_field('adress').max_length
        self.assertEquals(max_length, 100)


class ReceptionTests(TestCase):
    """
    тест модели Reception
    """

    def test_str(self):
        clinic = Clinic(name='Hatiko', city='Ekaterinburg', adress='Posadskay', number_phone=893586859,
                        email='hatiko@yandex.ru')
        breed = Breeds(name='Pug')
        pet = Pets(name='Rita', breed=breed, date_birth=date(2013, 9, 1))
        doctor = Doctors(name='Pork Alexey', specialty='optometrist', number_phone=8948385858)
        reception = Reception(name_Doctor=doctor, name_Clinic=clinic,
                              name_Pet=pet, purpose_of_visit = 'scheduled inspection')
        self.assertEquals(
            str(reception),
            'Pork Alexey optometrist 8948385858 Hatiko Ekaterinburg Posadskay 893586859 hatiko@yandex.ru Rita Pug 2013-09-01 scheduled inspection',
        )

    def test_purpose_of_visit_max_length(self):
        clinic = Clinic(name='Hatiko', city='Ekaterinburg', adress='Posadskay', number_phone=893586859,
                        email='hatiko@yandex.ru')
        breed = Breeds(name='Pug')
        pet = Pets(name='Rita', breed=breed, date_birth=date(2013, 9, 1))
        doctor = Doctors(name='Pork Alexey', specialty='optometrist', number_phone=8948385858)
        reception = Reception(name_Doctor=doctor, name_Clinic=clinic, name_Pet=pet,
                              purpose_of_visit='scheduled inspection')


        max_length = reception._meta.get_field('purpose_of_visit').max_length
        self.assertEquals(max_length, 254)


class MessageTests(TestCase):
    """
    тест модели Message
    """

    def test_str(self):
        breed1 = Breeds(name='Pug')
        pet1 = Pets(name='Rita', breed=breed1, date_birth=date(2013, 9, 1))

        breed2 = Breeds(name='Samoyed')
        pet2 = Pets(name='Tor', breed=breed2, date_birth=date(2018, 1, 12))

        meeting = Message(person_one=pet1, person_two=pet2, date_meeting = date(2020, 10, 11),
                          message = 'walk in the park')

        self.assertEquals(
            str(meeting),
            'Rita Pug 2013-09-01 Tor Samoyed 2018-01-12 2020-10-11 walk in the park',
        )
