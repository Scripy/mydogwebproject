"""webdog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from django.contrib.auth import views
from django.contrib.auth.views import LoginView

from . import views

urlpatterns = [
    #path(r'^$', views.page_index, name='page_index'),
    path('', views.page_index, name='page_index'),
    path('login/', views.page_login, name='page_login'),
    path('proc_login/', views.proc_login, name='proc_login'),
    #path('welcome/', views.welcome, name='welcome'),
    #path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('admin/', admin.site.urls),
    path('register/', views.RegisterFormView.as_view(), name='register'),
    path('dispatch/', views.dispatch, name='dispatch'),
    path('reg/', views.reg, name='reg'),
    path('success/', views.success, name='success'),
    path('change_pass/', views.change_pass, name='change_pass'),
    path('change/', views.change, name='change'),
    path('facts/', views.facts, name='facts'),

]
