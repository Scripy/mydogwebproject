from django.test import TestCase
from django.contrib.auth import authenticate, get_user_model

class SigninTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(username='pug', password='pugrita1234')
        self.user.save()
    def tearDown(self):
        self.user.delete()
    def test_correct(self):
        user = authenticate(username='pug', password='pugrita1234')
        self.assertTrue((user is not None) and user.is_authenticated)
    def test_wrong_username(self):
        user = authenticate(username='wrong', password='pugrita1234')
        self.assertFalse(user is not None and user.is_authenticated)
    def test_wrong_password(self):
        user = authenticate(username='pug', password='wrong')
        self.assertFalse(user is not None and user.is_authenticated)