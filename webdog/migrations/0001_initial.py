# Generated by Django 3.1.1 on 2020-10-01 13:29

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Clinic',
            fields=[
                ('name', models.CharField(max_length=100, primary_key=True, serialize=False)),
                ('city', models.CharField(max_length=100)),
                ('adress', models.CharField(max_length=100)),
                ('number_phone', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Doctors',
            fields=[
                ('name', models.CharField(max_length=100, primary_key=True, serialize=False)),
                ('specialty', models.CharField(max_length=30)),
                ('number_phone', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Reception',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
    ]
