# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
import datetime
from datetime import date

class Breeds(models.Model):
    """
    Породы
    """
    name = models.CharField(primary_key=True, max_length=30)
    weight = models.IntegerField()
    height = models.IntegerField()
    description = models.TextField()

    def __str__(self):
        return self.name

    def all_information_breed(self):
        return '%s %s %s ' % (self.name, self.weight, self.height)

class Pets(models.Model):
    """
    Питомцы
    """
    name = models.CharField(primary_key=True, max_length=30)
    breed = models.ForeignKey(Breeds, on_delete=models.CASCADE)
    date_birth = models.DateField(default=date.today, null=True, blank=True)
    weight = models.IntegerField()
    name_owner = models.ForeignKey(User, on_delete=models.CASCADE, to_field='id', db_column='owner_id', null=False)

    class Meta:
        db_table = "Pets"
        app_label = "Webdog"
        abstract = True

    def __str__(self):
        return '%s %s %s' % (self.name, self.breed, self.date_birth)

    def get_age(self):
        date_birth = self.date_birth
        today = date.today()
        return today.year - date_birth.year - ((today.month, today.day) < (date_birth.month, date_birth.day))

    def dog_class_by_age(self):
        if self.get_age() < 1:
            return "puppy"
        elif 1 <= self.get_age() <= 2:
            return "junior"
        elif 2 < self.get_age() < 8:
            return "open"
        else:
            return "veteran"


class Vaccination_certificate(models.Model):
    """
    Прививочный сертификат(возраст рекомендованных прививок)
    """
    name = models.CharField(primary_key=True, max_length=30)
    age = models.IntegerField()

    class Meta:
        abstract = True

    def __str__(self):
        return '%s %s' % (self.name, self.age)


class Vaccination_certificate_of_pet(models.Model):
    """
    Прививочный сертификат определенного питомца
    """
    name = models.ForeignKey(
        Pets,
        on_delete=models.CASCADE,
    )
    vaccination = models.ForeignKey(
        Vaccination_certificate,
        on_delete=models.CASCADE,
    )
    date = models.DateField(null=True, blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        return '%s %s' % (self.name, self.vaccination)


class Doctors(models.Model):
    """
    Врачи
    """
    name = models.CharField(max_length=100, primary_key=True)
    specialty = models.CharField(max_length=30)
    number_phone = models.IntegerField()

    def __str__(self):
        return '%s %s %s' % (self.name, self.specialty, self.number_phone)

class Clinic(models.Model):
    """
    Клиники
    """
    name = models.CharField(max_length=100, primary_key=True)
    city = models.CharField(max_length=100)
    adress = models.CharField(max_length=100)
    number_phone = models.IntegerField()
    email = models.EmailField()

    def __str__(self):
        return '%s %s %s %s %s' % (self.name, self.city, self.adress, self.number_phone, self.email)

class Doctors_and_Clinics(models.Model):
    """
    Клиники и работающие в них доктора
    """
    name_Doctor = models.ForeignKey(
        Doctors,
        on_delete=models.CASCADE,
    )
    name_Clinic = models.ForeignKey(
        Clinic,
        on_delete=models.CASCADE,
    )

class Reception(models.Model):
    """
    Прием питомца у врача в определенной клинике
    """
    name_Doctor = models.ForeignKey(
        Doctors,
        on_delete=models.CASCADE,
    )
    name_Clinic = models.ForeignKey(
        Clinic,
        on_delete=models.CASCADE,
    )
    name_Pet = models.ForeignKey(
        Pets,
        on_delete=models.CASCADE,
    )

    purpose_of_visit = models.CharField(max_length=254)
    data = models.DateField(null=True, blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        return '%s %s %s %s' % (self.name_Doctor, self.name_Clinic, self.name_Pet, self.purpose_of_visit)

class Message(models.Model):
    """
    Договоренность о встрече
    """
    person_one = models.ForeignKey(
        Pets,
        on_delete=models.CASCADE,
    )
    person_two = models.ForeignKey(
        Pets,
        on_delete=models.CASCADE,
    )
    date_meeting = models.DateField(default=date.today, null=True, blank=True)
    message = models.TextField()

    class Meta:
        abstract = True

    def __str__(self):
        return '%s %s %s %s' % (self.person_one, self.person_two, self.date_meeting, self.message)